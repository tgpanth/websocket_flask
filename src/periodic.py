import time

from threading import Thread, Event
from random import random

from app import socketio


class NotificationThread(Thread):
    def __init__(self):
        self.delay = 5
        self.stop_event = Event()
        super(NotificationThread, self).__init__()

    def run(self):
        while not self.stop_event.isSet():
            number = round(random()*100, 2)
            print('Send to client number: ', number)
            socketio.emit('message', number, namespace='/')
            time.sleep(self.delay)

    def stop(self):
        self.stop_event.set()


class ThreadManager:
    def __init__(self, class_name):
        self.thread = class_name()
        if not hasattr(self.thread, 'stop'):
            raise Exception('Stop method not found')
        self.thread.daemon = True
        self.thread.start()

    def isAlive(self):
        return self.thread.isAlive()

    def start(self):
        if self.isAlive:
            return
        self.thread.start()