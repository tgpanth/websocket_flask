from flask_socketio import send, emit
from app import socketio

from periodic import NotificationThread


@socketio.on('client_connected')
def handle_client_connect(json):
    print('Client connected: {0}'.format(str(json)))


@socketio.on('disconnect')
def handle_client_disconnect(json):
    print('Client disconnected: {0}'.format(str(json)))

@socketio.on('message', namespace='/')
def send_message(message):
    emit('message', {'data': message['data']})