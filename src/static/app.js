var address = 'http://' + document.domain + ':' + location.port
var socket = io.connect(address);

socket.on('connect', function() {
    // we emit a connected message to let knwo the client that we are connected.
    socket.emit('client_connected', {data: 'client_connected'});
	console.log('Connected to: ' + address)
});
socket.on('message', function(msg) {
	console.log('Got message: ' + msg)
})
socket.on('disconnect', function(msg) {
	console.log('Disconnected from: ' + address)
})