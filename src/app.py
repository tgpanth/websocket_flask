from flask import Flask
from flask_socketio import SocketIO

import eventlet
eventlet.monkey_patch()


app = Flask(__name__)
socketio = SocketIO(app)

from views import index

from websocket import (
      handle_client_connect,
      handle_client_disconnect,
)

from periodic import ThreadManager, NotificationThread

thread_manager = ThreadManager(NotificationThread)
app.thread_manager = thread_manager
app.thread_manager.start()